import '../aboutus/aboutus_widget.dart';
import '../clean_food_diet2/clean_food_diet2_widget.dart';
import '../clean_food_diet3/clean_food_diet3_widget.dart';
import '../flutter_flow/flutter_flow_theme.dart';
import '../flutter_flow/flutter_flow_util.dart';
import '../flutter_flow/flutter_flow_widgets.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePageWidget extends StatefulWidget {
  const HomePageWidget({Key key}) : super(key: key);

  @override
  _HomePageWidgetState createState() => _HomePageWidgetState();
}

class _HomePageWidgetState extends State<HomePageWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Color(0xFFFFFDE7),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: [
              Align(
                alignment: AlignmentDirectional(0, -0.31),
                child: Image.network(
                  'https://www.cleaneatingkitchen.com/wp-content/uploads/2019/02/platter-filled-with-real-food-ingredients.jpg',
                  width: MediaQuery.of(context).size.width * 2.5,
                  height: MediaQuery.of(context).size.height * 0.2,
                  fit: BoxFit.cover,
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.04, 0.19),
                child: FFButtonWidget(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.fade,
                        duration: Duration(milliseconds: 300),
                        reverseDuration: Duration(milliseconds: 300),
                        child: CleanFoodDiet2Widget(),
                      ),
                    );
                  },
                  text: 'อาหารเพื่อสุขภาพ',
                  options: FFButtonOptions(
                    width: 200,
                    height: 50,
                    color: Color(0xFFF9EB6D),
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 22,
                    ),
                    elevation: 10,
                    borderSide: BorderSide(
                      color: Color(0xFFF9EB6D),
                      width: 5,
                    ),
                    borderRadius: 35,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.07, 0.47),
                child: FFButtonWidget(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CleanFoodDiet3Widget(),
                      ),
                    );
                  },
                  text: 'เครื่องดื่มเพื่อสุขภาพ',
                  options: FFButtonOptions(
                    width: 200,
                    height: 50,
                    color: Color(0xFFF9EB6D),
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 22,
                    ),
                    elevation: 10,
                    borderSide: BorderSide(
                      color: Color(0xFFF9EB6D),
                      width: 5,
                    ),
                    borderRadius: 35,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.13, -0.73),
                child: Text(
                  'Clean Food Diet',
                  style: TextStyle(
                    color: Color(0xFFF69A23),
                    fontWeight: FontWeight.bold,
                    fontSize: 45,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.11, 0.73),
                child: FFButtonWidget(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AboutusWidget(),
                      ),
                    );
                  },
                  text: 'เกี่ยวกับเรา',
                  options: FFButtonOptions(
                    width: 200,
                    height: 50,
                    color: Color(0xFFF9EB6D),
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 22,
                    ),
                    elevation: 10,
                    borderSide: BorderSide(
                      color: Color(0xFFF9EB6D),
                      width: 5,
                    ),
                    borderRadius: 35,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CleanFoodDiet2Widget extends StatefulWidget {
  const CleanFoodDiet2Widget({Key key}) : super(key: key);

  @override
  _CleanFoodDiet2WidgetState createState() => _CleanFoodDiet2WidgetState();
}

class _CleanFoodDiet2WidgetState extends State<CleanFoodDiet2Widget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Color(0xFFFFFDE7),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: [
              Align(
                alignment: AlignmentDirectional(-0.92, -0.97),
                child: FFButtonWidget(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HomePageWidget(),
                      ),
                    );
                  },
                  text: 'Return',
                  icon: FaIcon(
                    FontAwesomeIcons.angleDoubleLeft,
                    color: Color(0xFFFB0C0C),
                    size: 15,
                  ),
                  options: FFButtonOptions(
                    width: 110,
                    height: 25,
                    color: Color(0xFFFDF287),
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    elevation: 10,
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 5,
                    ),
                    borderRadius: 35,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.04, 0.81),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 0, 0, 0),
                  child: FFButtonWidget(
                    onPressed: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => TofuSteakWidget(),
                        ),
                      );
                    },
                    text: 'สเต็กเต้าหู้',
                    options: FFButtonOptions(
                      width: 180,
                      height: 40,
                      color: Color(0xFF42EF49),
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                      elevation: 3,
                      borderSide: BorderSide(
                        color: Color(0xFF9AF89E),
                        width: 2,
                      ),
                      borderRadius: 10,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0, 0.62),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Image.network(
                    'https://img.kapook.com/u/2017/surauch/cooking/n1_3.jpg',
                    width: MediaQuery.of(context).size.width * 2,
                    height: MediaQuery.of(context).size.height * 0.15,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.01, 0.27),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 0, 0, 0),
                  child: FFButtonWidget(
                    onPressed: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              LemongrassandEringiMushroomSaladWidget(),
                        ),
                      );
                    },
                    text: 'ยำตะไคร้เห็ดออรินจิ',
                    options: FFButtonOptions(
                      width: 185,
                      height: 40,
                      color: Color(0xFF42EF49),
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                      elevation: 3,
                      borderSide: BorderSide(
                        color: Color(0xFF9AF89E),
                        width: 2,
                      ),
                      borderRadius: 10,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0, 0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Image.network(
                    'https://s359.kapook.com/pagebuilder/22572688-a3ca-473f-a849-5385c51ebb7f.jpg',
                    width: MediaQuery.of(context).size.width * 2,
                    height: MediaQuery.of(context).size.height * 0.15,
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0, -0.6),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Image.network(
                    'https://images.thaiza.com/186/186_20150602172810..jpg',
                    width: MediaQuery.of(context).size.width * 2,
                    height: MediaQuery.of(context).size.height * 0.15,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.08, -0.86),
                child: Text(
                  'อาหารเพื่อสุขภาพ',
                  style: FlutterFlowTheme.title1.override(
                    fontFamily: 'Poppins',
                    color: Color(0xFF0D47A1),
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.03, -0.28),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 0, 0, 0),
                  child: FFButtonWidget(
                    onPressed: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ChickenBreastSaladWidget(),
                        ),
                      );
                    },
                    text: 'สลัดอกไก่',
                    options: FFButtonOptions(
                      width: 130,
                      height: 40,
                      color: Color(0xFF42EF49),
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                      elevation: 3,
                      borderSide: BorderSide(
                        color: Color(0xFF9AF89E),
                        width: 2,
                      ),
                      borderRadius: 10,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CleanFoodDiet2Widget extends StatefulWidget {
  const CleanFoodDiet2Widget({Key key}) : super(key: key);

  @override
  _CleanFoodDiet2WidgetState createState() => _CleanFoodDiet2WidgetState();
}

class _CleanFoodDiet2WidgetState extends State<CleanFoodDiet2Widget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Color(0xFFFFFDE7),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: [
              Align(
                alignment: AlignmentDirectional(-0.92, -0.97),
                child: FFButtonWidget(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HomePageWidget(),
                      ),
                    );
                  },
                  text: 'Return',
                  icon: FaIcon(
                    FontAwesomeIcons.angleDoubleLeft,
                    color: Color(0xFFFB0C0C),
                    size: 15,
                  ),
                  options: FFButtonOptions(
                    width: 110,
                    height: 25,
                    color: Color(0xFFFDF287),
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    elevation: 10,
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 5,
                    ),
                    borderRadius: 35,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.04, 0.81),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 0, 0, 0),
                  child: FFButtonWidget(
                    onPressed: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => TofuSteakWidget(),
                        ),
                      );
                    },
                    text: 'สเต็กเต้าหู้',
                    options: FFButtonOptions(
                      width: 180,
                      height: 40,
                      color: Color(0xFF42EF49),
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                      elevation: 3,
                      borderSide: BorderSide(
                        color: Color(0xFF9AF89E),
                        width: 2,
                      ),
                      borderRadius: 10,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0, 0.62),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Image.network(
                    'https://img.kapook.com/u/2017/surauch/cooking/n1_3.jpg',
                    width: MediaQuery.of(context).size.width * 2,
                    height: MediaQuery.of(context).size.height * 0.15,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.01, 0.27),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 0, 0, 0),
                  child: FFButtonWidget(
                    onPressed: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              LemongrassandEringiMushroomSaladWidget(),
                        ),
                      );
                    },
                    text: 'ยำตะไคร้เห็ดออรินจิ',
                    options: FFButtonOptions(
                      width: 185,
                      height: 40,
                      color: Color(0xFF42EF49),
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                      elevation: 3,
                      borderSide: BorderSide(
                        color: Color(0xFF9AF89E),
                        width: 2,
                      ),
                      borderRadius: 10,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0, 0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Image.network(
                    'https://s359.kapook.com/pagebuilder/22572688-a3ca-473f-a849-5385c51ebb7f.jpg',
                    width: MediaQuery.of(context).size.width * 2,
                    height: MediaQuery.of(context).size.height * 0.15,
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0, -0.6),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Image.network(
                    'https://images.thaiza.com/186/186_20150602172810..jpg',
                    width: MediaQuery.of(context).size.width * 2,
                    height: MediaQuery.of(context).size.height * 0.15,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.08, -0.86),
                child: Text(
                  'อาหารเพื่อสุขภาพ',
                  style: FlutterFlowTheme.title1.override(
                    fontFamily: 'Poppins',
                    color: Color(0xFF0D47A1),
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.03, -0.28),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 0, 0, 0),
                  child: FFButtonWidget(
                    onPressed: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ChickenBreastSaladWidget(),
                        ),
                      );
                    },
                    text: 'สลัดอกไก่',
                    options: FFButtonOptions(
                      width: 130,
                      height: 40,
                      color: Color(0xFF42EF49),
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                      elevation: 3,
                      borderSide: BorderSide(
                        color: Color(0xFF9AF89E),
                        width: 2,
                      ),
                      borderRadius: 10,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class LemongrassandEringiMushroomSaladWidget extends StatefulWidget {
  const LemongrassandEringiMushroomSaladWidget({Key key}) : super(key: key);

  @override
  _LemongrassandEringiMushroomSaladWidgetState createState() =>
      _LemongrassandEringiMushroomSaladWidgetState();
}

class _LemongrassandEringiMushroomSaladWidgetState
    extends State<LemongrassandEringiMushroomSaladWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Color(0xFFFFFDE7),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: [
              Align(
                alignment: AlignmentDirectional(-0.9, 0.02),
                child: Text(
                  'วิธีทำ',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.9, -0.7),
                child: Text(
                  'ส่วนผสม',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.63, -0.8),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 5, 0, 0),
                  child: Icon(
                    Icons.access_time,
                    color: Colors.blue,
                    size: 30,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.92, -0.97),
                child: FFButtonWidget(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CleanFoodDiet2Widget(),
                      ),
                    );
                  },
                  text: 'Return',
                  icon: FaIcon(
                    FontAwesomeIcons.angleDoubleLeft,
                    color: Color(0xFFFB0C0C),
                    size: 15,
                  ),
                  options: FFButtonOptions(
                    width: 110,
                    height: 25,
                    color: Color(0xFFFDF287),
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    elevation: 10,
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 5,
                    ),
                    borderRadius: 35,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.49, -0.45),
                child: Text(
                  'เห็ดออรินจิหั่นชิ้น\nตะไคร้ซอย\nหอมแดงซอย\nน้ำมะนาว\nซีอิ๊วขาว\nน้ำตาลทราย\nผักชีซอย\nผักชีฝรั่งซอย\nพริกขี้หนูสวนซอย\nใบสะระแหน่ สำหรับโรย',
                  textAlign: TextAlign.start,
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 12,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.06, -0.89),
                child: Text(
                  'ยำตะไคร้เห็ดออรินจิ',
                  style: FlutterFlowTheme.title1,
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0, 0.54),
                child: Text(
                  '     1. ผสมน้ำมะนาว ซีอิ๊วขาว และน้ำตาลทราย คนให้ละลายเข้ากัน\n\n    2. ลวกเห็ดออรินจิในน้ำเดือดจนสุก  ตักขึ้น พักให้สะเด็ดน้ำ   \n     นำไปใส่ในส่วนผสมข้อที่ 1 \n\n     3. ใส่พริกขี้หนู ตะไคร้ หอมแดง ผักชี \n     และผักชีฝรั่ง คนให้เข้ากัน ตักใส่จาน โรยด้วยใบสะระแหน่ จัดเสิร์ฟ\n\n      เคล็ดลับ : ลวกเห็ดแค่พอสุกและนำไปแช่น้ำเย็นทันทีหลังจากลวก  \nเพื่อให้เห็ดมีรสสัมผัสเหนียวหนึบและเด้ง\n',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 12,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.04, -0.79),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 5, 0, 0),
                  child: Icon(
                    Icons.people_alt_sharp,
                    color: Color(0xFFF8910C),
                    size: 30,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.48, -0.45),
                child: Text(
                  '200 กรัม\n5 ต้น\n2 ช้อนโต๊ะ\n3 ช้อนโต๊ะ\n2 ช้อนโต๊ะ\n1 ช้อนโต๊ะ\n1 ช้อนโต๊ะ\n1 ช้อนโต๊ะ\n1 ช้อนโต๊ะ\n',
                  textAlign: TextAlign.start,
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 12,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.14, -0.78),
                child: Text(
                  '15 นาที              สำหรับ 2 คน',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(3.3, -0.96),
                child: FFButtonWidget(
                  onPressed: () {
                    print('Button pressed ...');
                  },
                  text: 'Button',
                  options: FFButtonOptions(
                    width: 130,
                    height: 40,
                    color: FlutterFlowTheme.primaryColor,
                    textStyle: FlutterFlowTheme.subtitle2.override(
                      fontFamily: 'Poppins',
                      color: Colors.white,
                    ),
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 1,
                    ),
                    borderRadius: 12,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TofuSteakWidget extends StatefulWidget {
  const TofuSteakWidget({Key key}) : super(key: key);

  @override
  _TofuSteakWidgetState createState() => _TofuSteakWidgetState();
}

class _TofuSteakWidgetState extends State<TofuSteakWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Color(0xFFFFFDE7),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: [
              Align(
                alignment: AlignmentDirectional(-0.9, -0.14),
                child: Text(
                  'วิธีทำ',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.9, -0.7),
                child: Text(
                  'ส่วนผสม',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.63, -0.8),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 5, 0, 0),
                  child: Icon(
                    Icons.access_time,
                    color: Colors.blue,
                    size: 30,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.92, -0.97),
                child: FFButtonWidget(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CleanFoodDiet2Widget(),
                      ),
                    );
                  },
                  text: 'Return',
                  icon: FaIcon(
                    FontAwesomeIcons.angleDoubleLeft,
                    color: Color(0xFFFB0C0C),
                    size: 15,
                  ),
                  options: FFButtonOptions(
                    width: 110,
                    height: 25,
                    color: Color(0xFFFDF287),
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    elevation: 10,
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 5,
                    ),
                    borderRadius: 35,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.37, -0.49),
                child: Text(
                  'เต้าหู้โมเมน\nแครอต\nเห็ด \nเนยจากน้ำมันทานตะวัน\nซอสปรุงรส (เจ)\nซอสเห็ดหอม\nน้ำตาลทราย\n',
                  textAlign: TextAlign.start,
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 12,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.06, -0.89),
                child: Text(
                  'สเต็กเต้าหู้',
                  style: FlutterFlowTheme.title1,
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.04, -0.79),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 5, 0, 0),
                  child: Icon(
                    Icons.people_alt_sharp,
                    color: Color(0xFFF8910C),
                    size: 30,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(2.98, 0.24),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(20, 0, 0, 0),
                  child: Text(
                    '      1. นำเนยน้ำมันทานตะวันไปละลายในกระทะ\n\n      2. ใส่เต้าหู้ลงไปทอดด้วยไฟกลางค่อนไปทางอ่อนให้ผิวเกรียม แล้วตักขึ้นพักไว้\n\n     3. ใส่เห็ดและแครอทลงไปผัดต่อในกระทะ\n\n     4. ปรุงรสด้วย  น้ำตาลทราย ซอสปรุงรส ซอสเห็ดหอม  \nเคี่ยวให้ข้นขึ้นเล็กน้อย แล้วตักราดเต้าหู้ เป็นอันเสร็จ',
                    style: FlutterFlowTheme.bodyText1.override(
                      fontFamily: 'Poppins',
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.14, -0.78),
                child: Text(
                  '30 นาที              สำหรับ 2 คน',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.51, -0.48),
                child: Text(
                  '1 ชิ้นใหญ๋\n50 กรัม\n50 กรัม\n2-3 ช้อนโต๊ะ\n1 ช้อนโต๊ะ\n1 ช้อนโต๊ะ\n1 ช้อนชา\n',
                  textAlign: TextAlign.start,
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 12,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(3.3, -0.96),
                child: FFButtonWidget(
                  onPressed: () {
                    print('Button pressed ...');
                  },
                  text: 'Button',
                  options: FFButtonOptions(
                    width: 130,
                    height: 40,
                    color: FlutterFlowTheme.primaryColor,
                    textStyle: FlutterFlowTheme.subtitle2.override(
                      fontFamily: 'Poppins',
                      color: Colors.white,
                    ),
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 1,
                    ),
                    borderRadius: 12,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CleanFoodDiet3Widget extends StatefulWidget {
  const CleanFoodDiet3Widget({Key key}) : super(key: key);

  @override
  _CleanFoodDiet3WidgetState createState() => _CleanFoodDiet3WidgetState();
}

class _CleanFoodDiet3WidgetState extends State<CleanFoodDiet3Widget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Color(0xFFFFFDE7),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: [
              Align(
                alignment: AlignmentDirectional(-0.92, -0.97),
                child: FFButtonWidget(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HomePageWidget(),
                      ),
                    );
                  },
                  text: 'Return',
                  icon: FaIcon(
                    FontAwesomeIcons.angleDoubleLeft,
                    color: Color(0xFFFB0C0C),
                    size: 15,
                  ),
                  options: FFButtonOptions(
                    width: 110,
                    height: 25,
                    color: Color(0xFFFDF287),
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    elevation: 10,
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 5,
                    ),
                    borderRadius: 35,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.04, 0.81),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 0, 0, 0),
                  child: FFButtonWidget(
                    onPressed: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CarrotCeleryJuiceWidget(),
                        ),
                      );
                    },
                    text: 'น้ำเซเลอรี่แครอท',
                    options: FFButtonOptions(
                      width: 180,
                      height: 40,
                      color: Color(0xFF42EF49),
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                      elevation: 3,
                      borderSide: BorderSide(
                        color: Color(0xFF9AF89E),
                        width: 2,
                      ),
                      borderRadius: 10,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0, 0.62),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Image.network(
                    'https://allrecipesdiy.com/wp-content/uploads/2019/10/3-35-1024x683.jpg',
                    width: MediaQuery.of(context).size.width * 2,
                    height: MediaQuery.of(context).size.height * 0.15,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.01, 0.27),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 0, 0, 0),
                  child: FFButtonWidget(
                    onPressed: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => TomatojuiceWidget(),
                        ),
                      );
                    },
                    text: 'น้ำมะเขือเทศ',
                    options: FFButtonOptions(
                      width: 160,
                      height: 40,
                      color: Color(0xFF42EF49),
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                      elevation: 3,
                      borderSide: BorderSide(
                        color: Color(0xFF9AF89E),
                        width: 2,
                      ),
                      borderRadius: 10,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0, 0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Image.network(
                    'https://www.girlsallaround.com/wp-content/uploads/2016/01/Food_Drinks_Glass_of_tomato_juice.jpg',
                    width: MediaQuery.of(context).size.width * 2,
                    height: MediaQuery.of(context).size.height * 0.15,
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0, -0.6),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Image.network(
                    'https://doodido.com/wp-content/uploads/2020/11/Drink-20-cover.jpg',
                    width: MediaQuery.of(context).size.width * 2,
                    height: MediaQuery.of(context).size.height * 0.15,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.08, -0.86),
                child: Text(
                  'เครื่องดื่มเพื่อสุขภาพ',
                  style: FlutterFlowTheme.title1.override(
                    fontFamily: 'Poppins',
                    color: Color(0xFF0D47A1),
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.03, -0.28),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 0, 0, 0),
                  child: FFButtonWidget(
                    onPressed: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LemongrassJuiceWidget(),
                        ),
                      );
                    },
                    text: 'น้ำตะไคร้มะนาว',
                    options: FFButtonOptions(
                      width: 160,
                      height: 40,
                      color: Color(0xFF42EF49),
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                      elevation: 3,
                      borderSide: BorderSide(
                        color: Color(0xFF9AF89E),
                        width: 2,
                      ),
                      borderRadius: 10,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(3.3, -0.96),
                child: FFButtonWidget(
                  onPressed: () {
                    print('Button pressed ...');
                  },
                  text: 'Button',
                  options: FFButtonOptions(
                    width: 130,
                    height: 40,
                    color: FlutterFlowTheme.primaryColor,
                    textStyle: FlutterFlowTheme.subtitle2.override(
                      fontFamily: 'Poppins',
                      color: Colors.white,
                    ),
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 1,
                    ),
                    borderRadius: 12,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class LemongrassJuiceWidget extends StatefulWidget {
  const LemongrassJuiceWidget({Key key}) : super(key: key);

  @override
  _LemongrassJuiceWidgetState createState() => _LemongrassJuiceWidgetState();
}

class _LemongrassJuiceWidgetState extends State<LemongrassJuiceWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Color(0xFFFFFDE7),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: [
              Align(
                alignment: AlignmentDirectional(-0.9, -0.14),
                child: Text(
                  'วิธีทำ',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.9, -0.7),
                child: Text(
                  'ส่วนผสม',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.63, -0.8),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 5, 0, 0),
                  child: Icon(
                    Icons.access_time,
                    color: Colors.blue,
                    size: 30,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.92, -0.97),
                child: FFButtonWidget(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CleanFoodDiet3Widget(),
                      ),
                    );
                  },
                  text: 'Return',
                  icon: FaIcon(
                    FontAwesomeIcons.angleDoubleLeft,
                    color: Color(0xFFFB0C0C),
                    size: 15,
                  ),
                  options: FFButtonOptions(
                    width: 110,
                    height: 25,
                    color: Color(0xFFFDF287),
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    elevation: 10,
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 5,
                    ),
                    borderRadius: 35,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.37, -0.49),
                child: Text(
                  ' ตะไคร้ \nน้ำเปล่า\nน้ำตาลทราย\nน้ำมะนาว',
                  textAlign: TextAlign.start,
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 12,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.04, 0.44),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(20, 0, 0, 0),
                  child: Text(
                    '          1. ล้างตะไคร้ให้สะอาด หั่นเป็นท่อน จากนั้นบุบให้แตก\n\n          2. ตั้งน้ำพร้อมใส่ตะไคร้ที่บุบแล้วลงไปต้มจนเดือด หรี่ไฟปานกลางต้มต่อไปสักพัก ปิดไฟ\n\n          3. กรองน้ำตะไคร้ที่ต้มแล้วด้วยผ้าขาวบาง จากนั้นจึงค่อยเติมน้ำตาลลงไปคนให้ละลาย (กะปริมาณตามความชอบ)\n\n          4. เพิ่มรสชาติด้วยน้ำมะนาวสักนิด เพียงบีบใส่แก้วที่ตักน้ำตะไคร้เตรียมไว้ คนให้เข้ากัน จะดื่มแบบร้อนหรือเย็นก็หอมสดชื่น ได้รสชาติอมเปรี้ยวอมหวาน อร่อยเหมือนกัน',
                    style: FlutterFlowTheme.bodyText1.override(
                      fontFamily: 'Poppins',
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.06, -0.89),
                child: Text(
                  'น้ำตะไคร้มะนาว',
                  style: FlutterFlowTheme.title1,
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.04, -0.79),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 5, 0, 0),
                  child: Icon(
                    Icons.people_alt_sharp,
                    color: Color(0xFFF8910C),
                    size: 30,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.31, -0.52),
                child: Text(
                  '3-5 ต้น\n3 ถ้วยตวง',
                  textAlign: TextAlign.start,
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 12,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.14, -0.78),
                child: Text(
                  '10 นาที              สำหรับ 1 คน',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(3.3, -0.96),
                child: FFButtonWidget(
                  onPressed: () {
                    print('Button pressed ...');
                  },
                  text: 'Button',
                  options: FFButtonOptions(
                    width: 130,
                    height: 40,
                    color: FlutterFlowTheme.primaryColor,
                    textStyle: FlutterFlowTheme.subtitle2.override(
                      fontFamily: 'Poppins',
                      color: Colors.white,
                    ),
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 1,
                    ),
                    borderRadius: 12,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TomatojuiceWidget extends StatefulWidget {
  const TomatojuiceWidget({Key key}) : super(key: key);

  @override
  _TomatojuiceWidgetState createState() => _TomatojuiceWidgetState();
}

class _TomatojuiceWidgetState extends State<TomatojuiceWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Color(0xFFFFFDE7),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: [
              Align(
                alignment: AlignmentDirectional(-0.9, -0.14),
                child: Text(
                  'วิธีทำ',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.9, -0.7),
                child: Text(
                  'ส่วนผสม',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.63, -0.8),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 5, 0, 0),
                  child: Icon(
                    Icons.access_time,
                    color: Colors.blue,
                    size: 30,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.92, -0.97),
                child: FFButtonWidget(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CleanFoodDiet3Widget(),
                      ),
                    );
                  },
                  text: 'Return',
                  icon: FaIcon(
                    FontAwesomeIcons.angleDoubleLeft,
                    color: Color(0xFFFB0C0C),
                    size: 15,
                  ),
                  options: FFButtonOptions(
                    width: 110,
                    height: 25,
                    color: Color(0xFFFDF287),
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    elevation: 10,
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 5,
                    ),
                    borderRadius: 35,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.37, -0.49),
                child: Text(
                  'มะเขือเทศท้อ\n สับปะรด \n มะนาว \n น้ำเปล่า',
                  textAlign: TextAlign.start,
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 12,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.13, 0.66),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(20, 0, 0, 0),
                  child: Text(
                    '          1. ล้างมะเขือเทศให้สะอาด หั่นเป็นชิ้น ๆ เตรียมไว้\n\n          2. นำมะเขือเทศใส่หม้อใช้ไฟกลาง หมั่นคนจนสุก ใช้เวลาประมาณ 20-30 นาที (ไม่ต้องเติมน้ำเปล่า หรือเกลือป่น)\n\n          3. นำมะเขือเทศต้มสุกมากรองเอาแต่น้ำ (จะได้น้ำมะเขือเทศสีแดงสวย)\n          เคล็ดลับ : ใช้ทัพพีช่วยบี้จะทำให้ได้น้ำมะเขือเทศเยอะขึ้น\n\n          4. ปอกเปลือกสับปะรด ล้างน้ำสะอาด หั่นเป็นชิ้น ๆ นำขึ้นตั้งไฟ พร้อมเติมน้ำเปล่า 2 ถ้วยตวงลงไปต้มจนสุก แล้วกรองเอาแต่น้ำ\n\n          5. ใส่น้ำมะนาว 1 ลูกลงไปในนำน้ำสับปะรด จากนั้นผสมน้ำสับปะรดที่ได้กับน้ำมะเขือเทศคนให้เข้ากัน\n\n          6. น้ำมะเขือเทศที่ได้นำไปบรรจุใส่ขวด เข้าตู้เย็นเก็บไว้ดื่มได้ 2 อาทิตย์',
                    style: FlutterFlowTheme.bodyText1.override(
                      fontFamily: 'Poppins',
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.06, -0.89),
                child: Text(
                  'น้ำมะเขือเทศ',
                  style: FlutterFlowTheme.title1,
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.28, -0.48),
                child: Text(
                  '2 กิโลกรัม\n1 ลูก\n1 ลูก\n2 ถ้วยตวง',
                  textAlign: TextAlign.start,
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 12,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.04, -0.79),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 5, 0, 0),
                  child: Icon(
                    Icons.people_alt_sharp,
                    color: Color(0xFFF8910C),
                    size: 30,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.14, -0.78),
                child: Text(
                  '10 นาที              สำหรับ 1 คน',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(3.3, -0.96),
                child: FFButtonWidget(
                  onPressed: () {
                    print('Button pressed ...');
                  },
                  text: 'Button',
                  options: FFButtonOptions(
                    width: 130,
                    height: 40,
                    color: FlutterFlowTheme.primaryColor,
                    textStyle: FlutterFlowTheme.subtitle2.override(
                      fontFamily: 'Poppins',
                      color: Colors.white,
                    ),
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 1,
                    ),
                    borderRadius: 12,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CarrotCeleryJuiceWidget extends StatefulWidget {
  const CarrotCeleryJuiceWidget({Key key}) : super(key: key);

  @override
  _CarrotCeleryJuiceWidgetState createState() =>
      _CarrotCeleryJuiceWidgetState();
}

class _CarrotCeleryJuiceWidgetState extends State<CarrotCeleryJuiceWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Color(0xFFFFFDE7),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: [
              Align(
                alignment: AlignmentDirectional(-0.9, -0.14),
                child: Text(
                  'วิธีทำ',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.9, -0.7),
                child: Text(
                  'ส่วนผสม',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.63, -0.8),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 5, 0, 0),
                  child: Icon(
                    Icons.access_time,
                    color: Colors.blue,
                    size: 30,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.92, -0.97),
                child: FFButtonWidget(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CleanFoodDiet3Widget(),
                      ),
                    );
                  },
                  text: 'Return',
                  icon: FaIcon(
                    FontAwesomeIcons.angleDoubleLeft,
                    color: Color(0xFFFB0C0C),
                    size: 15,
                  ),
                  options: FFButtonOptions(
                    width: 110,
                    height: 25,
                    color: Color(0xFFFDF287),
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    elevation: 10,
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 5,
                    ),
                    borderRadius: 35,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.04, 0.16),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(20, 0, 0, 0),
                  child: Text(
                    '  1. ใส่เซเลอรี่ แตงกวา แครอท และแอปเปิลลงในเครื่องปั่น ปั่นจนละเอียด กรองด้วยผ้าขาวบางคั้นเอาเฉพาะน้้ำ เตรียมไว้\n\n          2. เทน้ำเซเลอรี่ที่คั้นไว้ลงในแก้ว ตามด้วยน้ำสับปะรด คนผสมให้เข้ากัน ใส่น้ำแข็ง พร้อมดื่ม\n',
                    style: FlutterFlowTheme.bodyText1.override(
                      fontFamily: 'Poppins',
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.59, -0.49),
                child: Text(
                  'เซเลอรี่ (หั่นเป็นชิ้นเล็ก)\nแตงกวาเล็ก (หั่นเป็นชิ้นเล็ก)\n แครอท (ปอกเปลือกหั่นเป็นชิ้นเล็ก) \n แอปเปิล\nน้ำสับปะรด',
                  textAlign: TextAlign.start,
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 12,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.06, -0.89),
                child: Text(
                  'น้ำเซเลอรี่แครอท',
                  style: FlutterFlowTheme.title1,
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.04, -0.79),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(5, 5, 0, 0),
                  child: Icon(
                    Icons.people_alt_sharp,
                    color: Color(0xFFF8910C),
                    size: 30,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.14, -0.78),
                child: Text(
                  '10 นาที              สำหรับ 1 คน',
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 16,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.6, -0.48),
                child: Text(
                  '800 กรัม\n300 กรัม\n800 กรัม\n1 ลูก\n240 มิลลิลิตร',
                  textAlign: TextAlign.start,
                  style: FlutterFlowTheme.bodyText1.override(
                    fontFamily: 'Poppins',
                    fontSize: 12,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(3.3, -0.96),
                child: FFButtonWidget(
                  onPressed: () {
                    print('Button pressed ...');
                  },
                  text: 'Button',
                  options: FFButtonOptions(
                    width: 130,
                    height: 40,
                    color: FlutterFlowTheme.primaryColor,
                    textStyle: FlutterFlowTheme.subtitle2.override(
                      fontFamily: 'Poppins',
                      color: Colors.white,
                    ),
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 1,
                    ),
                    borderRadius: 12,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AboutusWidget extends StatefulWidget {
  const AboutusWidget({Key key}) : super(key: key);

  @override
  _AboutusWidgetState createState() => _AboutusWidgetState();
}

class _AboutusWidgetState extends State<AboutusWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Color(0xFFFFFDE7),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: [
              Align(
                alignment: AlignmentDirectional(0, -0.57),
                child: Image.network(
                  'https://scontent.fhdy2-1.fna.fbcdn.net/v/t1.6435-9/67830977_675509122860769_356504421829443584_n.jpg?_nc_cat=105&ccb=1-5&_nc_sid=174925&_nc_eui2=AeGPqIuAwSUpvqVhVx2G2WceD2rPcjhGu3MPas9yOEa7c_v8pPIE5idTAH8FaBynmacqjJlbULCHtWcbWWM_d2YQ&_nc_ohc=pgLHt14MhRAAX-F7_e9&_nc_ht=scontent.fhdy2-1.fna&oh=00_AT-JYKhzdVjHJd92DWM2RUI-lq_vxZU2WzFwVJAbbNI52A&oe=6229DFBC',
                  width: MediaQuery.of(context).size.width * 2.5,
                  height: MediaQuery.of(context).size.height * 0.2,
                  fit: BoxFit.cover,
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.06, 0.61),
                child: Text(
                  'นายอิรฟาน หามะ 6250110033',
                  style: FlutterFlowTheme.title1,
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.1, -0.18),
                child: Text(
                  'น.ส.สุธินี  แก้วมโณ 6250110015',
                  style: FlutterFlowTheme.title1,
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0, 0.37),
                child: Image.network(
                  'https://scontent.fhdy2-1.fna.fbcdn.net/v/t39.30808-6/242217082_3120152644919674_22165413141146379_n.jpg?_nc_cat=107&ccb=1-5&_nc_sid=8bfeb9&_nc_eui2=AeFb0SNKdENboF7D8BoJPbvcCmlH37Yb-pYKaUffthv6ljPwPFQBcwLNoY2e531BszqbXX9_k3jrZpReDw58PKBk&_nc_ohc=PPFoSLN75LEAX_8evrv&_nc_ht=scontent.fhdy2-1.fna&oh=00_AT-cHWtcmC_MWAS6XnN2hquRCDKAltywPvWiEXLQUq9icQ&oe=6208FCB1',
                  width: MediaQuery.of(context).size.width * 2.5,
                  height: MediaQuery.of(context).size.height * 0.2,
                  fit: BoxFit.cover,
                ),
              ),
              Align(
                alignment: AlignmentDirectional(0.11, -0.82),
                child: Text(
                  'ABOUT ME',
                  style: TextStyle(
                    color: Color(0xFFF69A23),
                    fontWeight: FontWeight.bold,
                    fontSize: 45,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional(-0.92, -0.97),
                child: FFButtonWidget(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HomePageWidget(),
                      ),
                    );
                  },
                  text: 'Return',
                  icon: FaIcon(
                    FontAwesomeIcons.angleDoubleLeft,
                    color: Color(0xFFFB0C0C),
                    size: 15,
                  ),
                  options: FFButtonOptions(
                    width: 110,
                    height: 25,
                    color: Color(0xFFFDF287),
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    elevation: 10,
                    borderSide: BorderSide(
                      color: Colors.transparent,
                      width: 5,
                    ),
                    borderRadius: 35,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
